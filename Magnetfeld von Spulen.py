# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import maabara as ma
import scipy as sp
import numpy as np
import matplotlib as mp
import matplotlib.pyplot as plt
mp.rcParams['text.usetex']=True
mp.rcParams['text.latex.unicode']=True
plt.rc('text', usetex=True)
plt.rc('font', family='serif')


###Eichung
eichung=np.loadtxt("eichung.dat")
eichungmittel=ma.data.student_t(np.transpose([eichung[:,1],eichung[:,2],eichung[:,3]]))

plt.clf()
fig, ax = plt.subplots(figsize=(10,6))
ax.ticklabel_format(style='sci', axis='both', scilimits=(-3,3))
x1=eichung[:,0]*6.58e-7*1e-3
y1=eichungmittel[:,0]
y_err1=eichungmittel[:,1]
m,b,tex=ma.linear_fit(x1,y1,y_err1)
ax.errorbar(x1, y1,y_err1,0,'r+',label='Messwerte')
x=np.linspace(min(x1),max(x1))
ax.plot(x, m.n*x+b.n,"b-", label ="$" + tex + "$",)
ax.legend(loc=2)
ax.set_xlabel(r'Ladung $Q$ [C]',fontsize=16)
ax.set_ylabel(r'Skalenwerte $P$ [skt]',fontsize=16)
plt.savefig("eichung.pdf", transparent=True, format="pdf", bbox_inches='tight')

# <codecell>

###lang Spule
langind=np.loadtxt('langind.dat')
langind=(langind-[27.2,0])*[-0.01,1]

Ladungsrechnung=ma.uncertainty.Sheet('P/m','Q')
Ladung=[]
i=0
for row in langind:
    Ladungsrechnung.v('P',langind[i,1],3)
    Ladungsrechnung.v('m',m.n,m.s,tex='\\kappa')
    test=Ladungsrechnung.p()
    Ladung.append([test.n,test.s])
    i+=1
Ladung1=np.vstack(Ladung)

# <codecell>

mag1=Ladung1*1/(369*np.pi*0.0295**2)*10000
langhall=np.loadtxt('langhall.dat')
langhall=((langhall-[24,5.3])*[-1e-2,1])*[1,1e-4]

plt.clf()
plt.figure(2)
plt.errorbar(langind[:,0],mag1[:,0],mag1[:,1],0.002,'ro',markersize=1,label='Messwerte Induktionsspule')
plt.errorbar(langhall[:,0],langhall[:,1],0,0.002,'bx',markersize=1,label='Messwerte Hall-Sonde')
plt.savefig("Magnetfeld1.pdf", transparent=True, format="pdf", bbox_inches='tight')

# <codecell>


# <codecell>

langhall

# <codecell>


